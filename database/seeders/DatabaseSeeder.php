<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
            
    		// $this->call(\RolesSeeder::class);
      //       $this->call(\UserTableSeeder::class);
            \App\Models\User::create([
            'name' => 'super admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 'admin'
        ]);

        \App\Models\User::create([
            'name' => 'editor',
            'email' => 'editor@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 'editor'
        ]);
        \App\Models\User::create([
            'name' => 'public user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 'user'
        ]);

    }
}
