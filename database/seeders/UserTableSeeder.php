<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\Models\User::create([
    		'name' => 'super admin',
    		'email' => 'admin@gmail.com',
    		'password' => bcrypt('123456'),
    		'role' => 'admin'
    	]);

    	\App\Models\User::create([
    		'name' => 'editor',
    		'email' => 'editor@gmail.com',
    		'password' => bcrypt('123456'),
    		'role' => 'editor'
    	]);
    	\App\Models\User::create([
    		'name' => 'public user',
    		'email' => 'user@gmail.com',
    		'password' => bcrypt('123456'),
    		'role' => 'user'
    	]);
    }
}
