<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$author = 
    	Role::create(['name'=>'Admin',
    		'slug'=>'author',
    		'permissions'=>json_encode(['create-post' =>true,
        'edit-post' =>true,
    'delete-post' =>true]),
    	]);
    	Role::create(['name'=>'Editor',
    		'slug'=>'editor',
    		'permissions'=>json_encode(['create-post' =>true,
        'edit-post' =>true]),
    	]);
    	Role::create(['name'=>'User',
    		'slug'=>'user',
    		'permissions'=>json_encode(['view-post' =>true]),
    	]);
    }
}
