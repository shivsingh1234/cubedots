<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/list', [App\Http\Controllers\PostController::class, 'index'])->name('list');
Route::get('/role/permission', [App\Http\Controllers\PostController::class, 'rolePermission'])->name('role');
Route::get('/post', [App\Http\Controllers\PostController::class, 'create'])->name('post');
Route::post('/user/post/send', [App\Http\Controllers\PostController::class, 'store'])->name('post-send');
Route::get('/post/edit/{id}', [App\Http\Controllers\PostController::class, 'edit'])->name('post.edit');
Route::post('/post/destroy/{id}', [App\Http\Controllers\PostController::class, 'destroy'])->name('post.destroy');
Route::post('/user/post/update/{id}', [App\Http\Controllers\PostController::class, 'update'])->name('post.update');