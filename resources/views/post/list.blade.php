

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        .form-control:focus {
            border-color: #000;
            box-shadow: none;
        }

        label {
            font-weight: 600;
        }

        .error {
            color: red;
            font-weight: 400;
            display: block;
            padding: 6px 0;
            font-size: 14px;
        }

        .form-control.error {
            border-color: red;
            padding: .375rem .75rem;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    
</div>
</body>
</html>
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
    @endif
<table id="table_id" class="display" style="width: 70%">
  @if(Auth::user()->role=='admin' || Auth::user()->role=='editor')
  &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('post')}}"><button type="button" class="btn btn-primary" style="margin-left: : 20%">Add</button></a>
  @endif
  @if(Auth::user()->role=='admin')
  &nbsp;<a href="#"><button type="button" class="btn btn-primary" style="margin-left: : 20%">Categories</button></a>
  @endif
  @if(Auth::user()->role=='admin')
  &nbsp;<a href="{{route('role')}}"><button type="button" class="btn btn-primary" style="margin-left: : 20%">roles&permission</button></a>
  @endif
  <thead>
    <tr style="text-align: center;">

      <th >Title</th>
      <th>Description</th>
      <th>Action</th>
  </tr>
</thead>
<tbody>
 @foreach($post as $post)
 <tr style="text-align: center;">
    <td >{{$post->title}}</td>
    <td>{{$post->description}}</td>
    @if(Auth::user()->role=='editor' || Auth::user()->role=='admin')
    <td style="width: 1%">
      <a href="{{ route('post.edit',$post->id)}}" class="btn btn-primary">Edit</a>
  </td>
  @endif
  @if(Auth::user()->role=='admin')
  <td style="width: 1%">
      <form action="{{ route('post.destroy', $post->id)}}" method="post">
        @csrf

        <button class="btn btn-danger" type="submit">Delete</button>
    </form>
</td>
@endif
</tr>
@endforeach
</tbody>
</table>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" ></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>

<script type="text/javascript">

  $(document).ready( function () {
    $('#table_id').DataTable();
} );


</script>
