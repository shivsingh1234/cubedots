<?php 
namespace App\Traits;

use DB;
use File;
use Image;

trait ImageUpload
{

   

        public static function imageSizes($folder){
            
            $img_sizes = array();
            
            switch($folder){
                case 'customer_profiles' :
                    $img_sizes['thumbnail'] = array('width'=>300, 'height'=>300, 'folder'=>'/thumb');
                    $img_sizes['medium'] = array('width'=>600, 'height'=>600, 'folder'=>'/medium');
                    break;
                case 'company_profile' :
                    $img_sizes['thumbnail'] = array('width'=>300, 'height'=>300, 'folder'=>'/thumb');
                    $img_sizes['medium'] = array('width'=>600, 'height'=>600, 'folder'=>'/medium');
                    break;  
            }
                
            return $img_sizes;
        }
    
        public  function makeDirs($folder='', $mode=0755, $defaultFolder='uploads/'){
    
            $path = public_path($defaultFolder.$folder);
           
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }
           
        }
    
        public  function uploadImage($request,$folder){
    
            $realpath = 'uploads/';
            self::makeDirs($folder);
    
            $img_sizes_arr = self::imageSizes($folder);  //predefined sizes in model
            $destinationPath = public_path($realpath.$folder);
          
            //get name of file
            $imagename = time().'.'.$request->file->getClientOriginalExtension();
            //store original image in specified folder
            $request->file->move($destinationPath, $imagename);

            //store resize image in medium and thumb folder. if folder not exists it will create folder
            foreach($img_sizes_arr as $k=>$v){
                // create resize sub-folder
                $sub_folder = $folder.$v['folder'];
                self::makeDirs($sub_folder);
    
                $destinationPathThumb = public_path($realpath.$sub_folder);

                //Resize image .it get real image and resize them.
                $img = Image::make($destinationPath.'/'.$imagename)->resize($v['width'], $v['height'], function ($constraint) {
                   
                    $constraint->upsize();
                });
                
                $img->save($destinationPathThumb.'/'.$imagename);
            }
    
           return array('name'=>$imagename); 
        }
    
        public static function deleteImage($folder,$file){
    
            $img_sizes_arr = self::imageSizes($folder);  //predefined sizes in model
            $realpath = 'uploads/';
    
            foreach($img_sizes_arr as $k=>$v){
    
                $sub_folder = $folder.$v['folder'];
                unlink( public_path($realpath.$sub_folder.'/'.$file) );
    
            }
    
            unlink( public_path($realpath.$folder.'/'.$file) );
    
        }
    
   

}