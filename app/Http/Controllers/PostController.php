<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Post;
use Auth;
class PostController extends Controller
{
  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->role=='admin'){
          $post = Post::all();  
        }
        else{
           $post = Post::where('user_id',Auth::user()->id)->get();   
        }
     
     return view('post.list',compact('post'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $users = User::latest()->get();
       return view('post.add',compact('users'));
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $request->validate([
        'title' => 'required',
        'description' => 'required',
          'image' => 'required|image',

     
    ]);

       $id = Post::max('id');
       $slug = Str::slug($request->title.$id, '-');
        if ($files = $request->file('image')) {
            //dd('gf');
         $destinationPath = public_path('/profile_images/'); // upload path
         $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
         $files->move($destinationPath, $profileImage);
         $insert['image'] = "$profileImage";
         Post::create(['title'=>$request->title,
          'user_id'=>Auth::user()->id,
          'slug'=>$slug,
          'description'=>$request->description,
          'featured_image'=>"$profileImage"

      ]);

     }
      return redirect()->route('list')->with('success', 'Post Created Successfully');  
 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $post = Post::where('id',$id)->first();
           //dd($post);
       return view('post.edit',compact('post'));
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      Post::where('id',$id)->update(['title'=>$request->title,
          'user_id'=>Auth::user()->id,
      //'slug'=>Auth::user()->id,
          'description'=>$request->description,
          'featured_image'=>$request->featured_image

      ]);

      return redirect()->route('list')->with('success', 'Post Created Successfully'); 
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     $post = Post::find($id);
     $post->delete();

     return redirect('/list')->with('success', 'Post deleted!');
 }
 public function rolePermission(Request $request){
 if(Auth::user()->role=='admin'){
          $user = User::all();  
        }
        else{
         $user = User::all();  
        }
     
     return view('role_permission',compact('user'));
 }
}
