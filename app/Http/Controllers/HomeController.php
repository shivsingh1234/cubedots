<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
          if(Auth::user()->role=='admin'){
          $post = Post::all();  
        }
        else{
           $post = Post::where('user_id',Auth::user()->id)->get();   
        }
     return view('post.list',compact('post'));
    }
}
